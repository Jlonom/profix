<?php
namespace App;

use ErrorException;
use stdClass;

/**
 * Class App
 * @package App
 */
class App {

    private array $_fileContentLines;

    public array $euroISOMap = [
        'AT', 'BE', 'BG', 'CY', 'CZ', 'DE',
        'DK', 'EE', 'ES', 'FI', 'FR', 'GR',
        'HR', 'HU', 'IE', 'IT', 'LT', 'LU',
        'LV', 'MT', 'NL', 'PO', 'PT', 'RO',
        'SE', 'SI', 'SK'
    ];

    public function __construct( $filename )
    {
        $handle = fopen($filename, "r");
        while(!feof($handle) && ($line = fgets($handle)) !== false) {
            $this->_fileContentLines[] = json_decode($line);
        }
    }

    public function process() :void
    {
        foreach($this->_fileContentLines as $transactionInfo) {
            $binInfo = $this->getBinInfo($transactionInfo->bin);

            if($binInfo) {
                $isEu = $this->getIsEu($binInfo->country->alpha2);
                $fixedAmount = $this->getFixedAmount($transactionInfo);
                $fullAmount = $fixedAmount * ($isEu ? 0.01 : 0.02);
                echo ceil($fullAmount * 100) / 100 . "\n";
            }
        }
    }

    /**
     * @param string $bin
     * @return stdClass|bool
     */
    public function getBinInfo(string $bin) :stdClass|bool
    {
        try {
            $binResults = $this->getRemoteContent('https://lookup.binlist.net/' . $bin);
            return json_decode($binResults);
        } catch (ErrorException $e) {
            echo $e->getMessage() . "\n";
            return false;
        }
    }

    /**
     * @param stdClass $transactionInfo
     * @return float
     */
    public function getFixedAmount(stdClass $transactionInfo) :float
    {
        $rate = $this->getRate($transactionInfo->currency);
        if($rate !== false) {
            if($transactionInfo->currency === 'EUR' || $rate === 0) {
                return (float)$transactionInfo->amount;
            }

            return (float)$transactionInfo->amount / $rate;
        }

        return 0;
    }

    /**
     * @param string $currency
     * @return string
     */
    public function getRate(string $currency) :string
    {
        try {
            $latestExchange = json_decode($this->getRemoteContent('https://api.exchangeratesapi.io/latest'));
            return isset($latestExchange->rates->{$currency}) ? (float)$latestExchange->rates->{$currency} : 1;
        } catch (ErrorException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * @param string $url
     * @return string
     * @throws ErrorException
     */
    public function getRemoteContent(string $url) :string
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $url);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,30);
        $response = curl_exec($curl);
        curl_close($curl);
        if(!$response) {
            throw new ErrorException(sprintf('Could not connect to url `%s`', $url));
        }
        return $response;
    }

    /**
     * @param string $country
     * @return bool
     */
    public function getIsEu(string $country) :bool
    {
        if(in_array($country, $this->euroISOMap)) {
            return true;
        }
        return false;
    }
}
