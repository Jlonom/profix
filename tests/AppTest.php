<?php


namespace Tests;

use PHPUnit\Framework\TestCase;
use App\App;

class AppTest extends TestCase
{
    private App $app;

    protected function setUp(): void
    {
        $this->app = new App(realpath(__DIR__) . '/../input.txt');
    }

    public function testGetBinInfo()
    {
        $this->assertEquals($this->app->getBinInfo('45717360')->scheme, 'visa');
        $this->assertNotEquals($this->app->getBinInfo('45417360')->country->currency, 'USD');
        $this->assertEquals($this->app->getBinInfo('9876'), false);
    }

    public function testGetFixedAmount()
    {
        $this->assertEquals(
            $this->app->getFixedAmount((object)['amount' => '100', 'currency' => 'EUR']), 100
        );

        $this->assertTrue(
            $this->app->getFixedAmount((object)['amount' => '100', 'currency' => 'USD']) < 100
        );
    }

    public function testGetRate()
    {
        $this->assertEquals($this->app->getRate('EUR'), 1);
        $this->assertTrue($this->app->getRate('USD') > 1);
        $this->assertFalse($this->app->getRate('GBP') > 1);
    }

    public function testGetIsEu()
    {
        $this->assertTrue($this->app->getIsEu('LT'));
        $this->assertFalse($this->app->getIsEu('JP'));
    }
}